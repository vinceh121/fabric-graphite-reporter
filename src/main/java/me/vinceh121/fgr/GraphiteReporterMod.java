package me.vinceh121.fgr;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.MetricSet;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;

public class GraphiteReporterMod implements ModInitializer {
	private final MetricRegistry registry = new MetricRegistry();

	@Override
	public void onInitialize() {
		final FgrConfig config;
		try {
			final File cfgFile = new File("./config/fgr.json");
			config = new Gson().fromJson(new FileReader(cfgFile), FgrConfig.class);
		} catch (final JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			throw new RuntimeException(e);
		}

		final Graphite graphite = new Graphite(config.getHost(), config.getPort());
		final GraphiteReporter report = GraphiteReporter.forRegistry(this.registry).build(graphite);

		ServerLifecycleEvents.SERVER_STARTED.register((srv) -> {
			System.out.println("Registering metrics...");
			registry.registerAll(config.getPrefix(), this.setupMetrics(srv));

			report.start(config.getInterval(), TimeUnit.SECONDS);
		});

		ServerLifecycleEvents.SERVER_STOPPING.register((srv) -> {
			System.out.println("Doing one last report and unregistering metrics");
			report.stop();
			report.report();
			this.registry.getMetrics().keySet().forEach(this.registry::remove); // clear() methods are overrated
		});
	}

	private MetricSet setupMetrics(final MinecraftServer srv) {
		final MetricRegistry set = new MetricRegistry();
		set.gauge("players.online", () -> srv::getCurrentPlayerCount);
		set.gauge("players.max", () -> srv::getMaxPlayerCount);

		for (final ServerWorld w : srv.getWorlds()) {
			set.gauge("world." + w.getRegistryKey().toString() + ".players", () -> w.getPlayers()::size);
			set.gauge("world." + w.getRegistryKey().toString() + ".blockentities", () -> w.blockEntities::size);
			set.gauge("world." + w.getRegistryKey().toString() + ".chunks.loaded",
					() -> w.getChunkManager()::getTotalChunksLoadedCount);
			set.gauge("world." + w.getRegistryKey().toString() + ".chunks.pendingtasks",
					() -> w.getChunkManager()::getPendingTasks);
		}

		set.gauge("tick.ticks", () -> srv::getTicks);
		set.gauge("tick.ticktime", () -> srv::getTickTime);

		set.gauge("taskcount", () -> srv::getTaskCount);

		return set;
	}
}
